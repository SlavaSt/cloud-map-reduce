/* 9/24/14 */
package cloud.reduce;

import cloud.Message;

/**
 * @author Slava Stashuk
 */
public interface ReduceMsg {

    class NewValue<V> extends Message {

        private V value;

        public NewValue(V value) {
            this.value = value;
        }

        public V getValue() {
            return value;
        }
    }

    class GetResult extends Message {

        public GetResult() {
            super();
        }

    }

    class Result<V> extends Message {

        private final V result;

        public Result(GetResult parent, V result) {
            super(parent);
            this.result = result;
        }

        public V getResult() {
            return result;
        }
    }

    class Ack extends Message {

    }
}
