/* 9/24/14 */
package cloud.reduce;

import akka.actor.UntypedActor;
import domain.ReduceFunction;

/**
 * @author Slava Stashuk
 */
public class ReduceService<V> extends UntypedActor {

    private final ReduceFunction<V> reduceFun;
    private V result;

    public ReduceService(ReduceFunction<V> reduceFun, V zero) {
        this.reduceFun = reduceFun;
        this.result = zero;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ReduceMsg.NewValue) {
            final ReduceMsg.NewValue<V> reduceTask = (ReduceMsg.NewValue<V>) message;
            V value = reduceTask.getValue();
            result = reduceFun.apply(result, value);
            sender().tell(new ReduceMsg.Ack(), self());
        } else if (message instanceof ReduceMsg.GetResult) {
            getSender().tell(new ReduceMsg.Result<>((ReduceMsg.GetResult) message, result), self());
        }
    }
}
