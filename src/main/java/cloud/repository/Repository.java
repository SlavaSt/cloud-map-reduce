package cloud.repository;/* 9/23/14 */

import akka.actor.UntypedActor;
import com.google.common.collect.ImmutableList;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author Slava Stashuk
 */
public class Repository<K, V> extends UntypedActor {

    private Map<K, V> store = new ConcurrentHashMap<>();

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof RepositoryMsg.Put) {
            RepositoryMsg.Put<K, V> putMessage = (RepositoryMsg.Put<K, V>) message;
            put(putMessage.getKey(), putMessage.getValue());
        } else if (message instanceof RepositoryMsg.GetAll) {
            sender().tell(new RepositoryMsg.GetAllResult<>(getAll()), self());
        }
    }

    public void put(K key, V value) {
        V oldValue = store.putIfAbsent(key, value);
        if (oldValue != null) {
            throw new RuntimeException("Object with key " + key + " is already in the store.");
        }
    }

    public List<V> getAll() {
        return ImmutableList.copyOf(store.values());
    }


    // TODO is it needed now?
    public List<V> query(Predicate<V> predicate) {
        return store.values().stream().filter(predicate).collect(Collectors.toList());
    }

}
