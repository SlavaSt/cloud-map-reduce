package cloud.repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author Slava Stashuk
 */
public interface RepositoryMsg {

    public static class Put<K, V> implements Serializable {
        private K key;
        private V value;

        public Put(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    public static class GetAll implements Serializable {
    }

    public static class GetAllResult<V> implements Serializable {
        private List<V> result;

        public GetAllResult(List<V> result) {
            this.result = result;
        }
    }
}
