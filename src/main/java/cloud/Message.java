/* 9/24/14 */
package cloud;

import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Slava Stashuk
 */
public class Message implements Serializable {

    private final UUID id;
    private final Optional<UUID> parentId;

    public Message(Message parent) {
        this(UUID.randomUUID(), Optional.of(parent.id()));
    }

    public Message() {
        this(UUID.randomUUID(), Optional.empty());
    }

    private Message(UUID id, Optional<UUID> parentId) {
        this.id = id;
        this.parentId = parentId;
    }

    public UUID id() {
        return id;
    }

}
