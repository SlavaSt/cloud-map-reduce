/* 9/24/14 */
package cloud.map;

import akka.actor.UntypedActor;
import domain.MapFunction;
import cloud.repository.Repository;

import java.util.List;

/**
 * @author Slava Stashuk
 */
public class MapService<K, V> extends UntypedActor {

    private Repository<K, V> repo;

    public MapService(Repository<K, V> repo) {
        this.repo = repo;
    }


    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof MapMsg.MapTask) {
            MapMsg.MapTask mapTask = (MapMsg.MapTask) message;
            final Object result = map(mapTask.mapFunction());
            sender().tell(new MapMsg.Result<>(result), self());
        }
    }

    private <R> R map(MapFunction<V, R> mapFun) {
        List<V> elements = repo.getAll();
        return mapFun.apply(elements);
    }
}
