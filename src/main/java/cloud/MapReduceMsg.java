/* 9/24/14 */
package cloud;

import domain.MapFunction;
import domain.ReduceFunction;

import java.io.Serializable;

/**
 * @author Slava Stashuk
 */
public interface MapReduceMsg {

    class MapReduce<V, R> implements Serializable {
        private final MapFunction<V, R> mapFun;
        private final ReduceFunction<R> reduceFun;
        private final R zero;

        public MapReduce(MapFunction<V, R> mapFun, ReduceFunction<R> reduceFun, R zero) {
            this.mapFun = mapFun;
            this.reduceFun = reduceFun;
            this.zero = zero;
        }
    }

    class Result<V> implements Serializable {
        public Result(V result) {
            this.result = result;
        }

        private final V result;
    }
}
