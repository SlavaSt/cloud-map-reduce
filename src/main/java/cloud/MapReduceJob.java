/* 9/24/14 */
package cloud;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import cloud.map.MapMsg;
import cloud.reduce.ReduceMsg;
import cloud.reduce.ReduceService;
import domain.MapFunction;
import domain.ReduceFunction;

import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Slava Stashuk
 */
public class MapReduceJob<V, R> extends UntypedActor {

    private final Collection<ActorRef> mapServices;

    private final MapFunction<V, R> mapFun;

    private final ReduceFunction<R> reduceFun;

    private final R zero;

    private ActorRef reduceService;

    private AtomicInteger reduceCounter;

    public MapReduceJob(Collection<ActorRef> mapServices, MapFunction<V, R> mapFun, ReduceFunction<R> reduceFun, R zero) {
        this.mapServices = mapServices;
        this.mapFun = mapFun;
        this.reduceFun = reduceFun;
        this.reduceCounter = new AtomicInteger(mapServices.size());
        this.zero = zero;
    }

    @Override
    public void preStart() {
        reduceService = context().system().actorOf(Props.create(ReduceService.class, reduceFun, zero));
        for (ActorRef mapService : mapServices) {
            mapService.tell(new MapMsg.MapTask<>(mapFun), self());
        }
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof MapMsg.Result) {
            MapMsg.Result mapResult = (MapMsg.Result) message;
            reduceService.tell(new ReduceMsg.NewValue(mapResult.getResult()), self());
        } else if (message instanceof ReduceMsg.Ack) {
            if (reduceCounter.decrementAndGet() == 0) {
                reduceService.tell(new ReduceMsg.GetResult(), self());
            }
        } else if (message instanceof ReduceMsg.Result) {
            ReduceMsg.Result reduceResult = (ReduceMsg.Result) message;
            getContext().parent().tell(new MapReduceMsg.Result<>(reduceResult.getResult()), self());
        }
    }
}
