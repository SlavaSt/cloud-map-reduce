/* 9/23/14 */
package domain;

import java.util.function.BiFunction;

/**
 * @author Slava Stashuk
 */
public interface ReduceFunction<V> extends BiFunction<V, V, V> {
}
