/* 9/23/14 */
package domain;

import java.util.Collection;
import java.util.function.Function;

/**
 * @author Slava Stashuk
 */
public interface MapFunction<V, R> extends Function<Collection<V>, R> {
}
